
from fastapi import FastAPI, HTTPException
from fastapi.encoders import jsonable_encoder

from datetime import datetime
from bs4 import BeautifulSoup

import requests
import calendar
import locale

app = FastAPI()

encabezado ={
    "user-agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36 OPR/38.0.2220.41"
}

@app.get("/")
async def root():
    return {"message": "Hello World"}

@app.get('/prueba')
def api():
    url = "https://www.ejemplo.ejemplo/"

    response = requests.get(url)

    if response.status_code == 200:
        #Creamos un objeto BeautifulSoup para analizar el contenido HTML de la página web:
        soup = BeautifulSoup(response.text, 'html.parser')

        # Extraer todas las etiquetas <img> en una lista
        img_tags = [img['src'] for img in soup.find_all('img')]

        # Extraer todas las etiquetas <meta> en una lista
        meta_tags = [str(meta) for meta in soup.find_all('meta')]

        # Extraer todos los formularios en una lista
        form_tags = [str(form) for form in soup.find_all('form')]

        # Extraer todos los enlaces
        a_tags = [link.get('href') for link in soup.find_all('a')]

        # Creamos un diccionario con los datos
        data = {
            "img_tags": img_tags,
            "meta_tags": meta_tags,
            "form_tags": form_tags,
            "a_tags": a_tags
        }

        return data

